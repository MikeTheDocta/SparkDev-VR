// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VR_Dev : ModuleRules
{
	public VR_Dev(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
