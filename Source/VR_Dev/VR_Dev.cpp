// Copyright Epic Games, Inc. All Rights Reserved.

#include "VR_Dev.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VR_Dev, "VR_Dev" );
 